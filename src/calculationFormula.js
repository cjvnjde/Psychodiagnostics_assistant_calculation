import {Parser} from 'hot-formula-parser';
import {countAll} from './calculationSchema.js';
const parser = new Parser();

export default function init() {

    const calc = document.getElementById('calc-formula');

    calc.addEventListener('click', ()=> {
		
        parseAll();
    });
}


export function parseAll(){
    setAllVariables();
    countAll();
    const allFormulaContainer = document.getElementsByClassName('formula-container');
    if(allFormulaContainer!== null){
        for(let i = 0; i < allFormulaContainer.length; i++){
            const formulaInput = allFormulaContainer[i].querySelector('.formula');
            const formulaResult = allFormulaContainer[i].querySelector('.formula-result');
            formulaResult.innerText = parser.parse(formulaInput.value).result;
        }
    }
    //formulaResult.innerText = parser.parse(formula.value).result;
}

function setAllVariables() {
    const allSchemas = document.getElementsByClassName('saved-schema');
    const countType = localStorage.getItem('type');
    if(countType === 'num'){
        for(let i = 0; i < allSchemas.length; i++){

            let varName = allSchemas[i].id;
            varName = varName.substring(0, varName.length - 6);

            const result = allSchemas[i].querySelector('.result');
            const val = result.innerText|0;
            parser.setVariable(varName.trim(), val);
        }

    }else if(countType ==='bool'){
        for(let i = 0; i < allSchemas.length; i++){

            let varName = allSchemas[i].id;
            varName = varName.substring(0, varName.length - 6);

            const result = allSchemas[i].querySelector('.result');
            const val = result.innerText|0;

            parser.setVariable(varName, val);
        }
    }
}


