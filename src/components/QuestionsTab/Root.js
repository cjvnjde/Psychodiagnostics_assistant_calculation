'use strict';
import React, {Conponent} from 'react';
import QuestionList from './QuestionList';
import store from '../../store/index';
import {Provider} from 'react-redux';

function Root(props) {
    return (
        <Provider store = {store}>
            <QuestionList />
        </Provider>
    );
}


export default Root;