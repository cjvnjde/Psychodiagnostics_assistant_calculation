/* eslint-disable no-mixed-spaces-and-tabs */
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getStatistic} from "../../ActionCreator";

class QuestionTypeYesNo extends Component {
    static propTypes = {
        maxNum: PropTypes.number,
        number: PropTypes.number,
        getStatistic: PropTypes.func
    };

    constructor(props) {
        super(props);
        this.state = {
            disabledPlus: false,
            disabledMinus: false
        };
    }


    render() {
        return (
            <div>
                <div>
                    <label className='label-plus'>Да</label>
                    <input className='plus-check' type='checkbox'
                        onChange={this.disableMinus}
                        disabled={this.state.disabledPlus} />
                </div>
                <div>
                    <label className='label-minus'>Нет</label>
                    <input className='minus-check' type='checkbox'
                        onChange={this.disablePlus}
                        disabled={this.state.disabledMinus} />
                </div>
            </div>
        );
    }

	disablePlus = () => {
	    this.setState({
	        disabledPlus: !this.state.disabledPlus
	     });
	    if(!this.state.disabledPlus){
            this.props.getStatistic(this.props.number, {minus: 1, plus: 0, num: 0});
        }else{
            this.props.getStatistic(this.props.number, {minus: 0, plus: 0, num: 0});
        }
	};
	
	disableMinus = () => {
	    this.setState({
	        disabledMinus: !this.state.disabledMinus
	    });
        if(!this.state.disabledMinus){
            this.props.getStatistic(this.props.number, {minus: 0, plus: 1, num: 0});
        }else{
            this.props.getStatistic(this.props.number, {minus: 0, plus: 0, num: 0});
        }

	};
}

export default connect(null, {getStatistic})(QuestionTypeYesNo);