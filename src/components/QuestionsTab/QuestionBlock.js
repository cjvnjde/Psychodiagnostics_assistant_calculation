import React, {Component} from 'react';
import PropTypes from 'prop-types';

import QuestionTypeYesNo from './QuestionTypeYesNo';
import QuestionTypeNumber from './QuestionTypeNumber';
import QuestionTypeNone from './QuestionTypeNone';

import {connect} from 'react-redux';
import {getType} from "../../ActionCreator";
import type from "../../reducer/type";

class QuestionBlock extends Component {
    static propTypes = {
        text: PropTypes.string,
        number: PropTypes.number,
        type: PropTypes.string
    };

    render() {
        const {text, number, type} = this.props;
        let question = <QuestionTypeNumber number={number}/>;
        if(type === 'bool'){
            question = <QuestionTypeYesNo number={number}/>;
        } else if(type === 'none'){
            question = <QuestionTypeNone />;
        }

        return (
            <div className='text-block'>
                <div className='text-line'>
                    <div className='num-block'>{number}</div>
                    <div className='text-t'>{text}</div>
                </div>
                {question}
            </div>
        );
    }
}

export default connect(state => ({
    type: state.type
}), {getType})(QuestionBlock);

