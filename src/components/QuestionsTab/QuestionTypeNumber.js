import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getStatistic} from '../../ActionCreator';

class QuestionTypeNumber extends Component {
    static propTypes = {
        maxNum: PropTypes.number,
        number: PropTypes.number,
        getStatistic: PropTypes.func
    };
    componentWillMount() {
        this.props.getStatistic(this.props.number, {minus: 0, plus: 0, num: 1});
    }
    render() {
        const {maxNum, number} = this.props;
        let options = [];
        for(let i = 0; i < maxNum; i++){
            options.push(
                <option value={i+1}
                    key={i.toString()+''+number.toString()}>{i+1}</option>
            );
        }
        return (
            <div>
                <select className='num-check' onChange={this.handleChange}>
                    {options}
                </select>
            </div>
        );
    }

    handleChange = (el) => {
        const val = parseInt(el.target.value);
        this.props.getStatistic(this.props.number, {minus: 0, plus: 0, num: val});
    }
}

export default connect(state => ({
    maxNum: state.maxNum
}), {getStatistic})(QuestionTypeNumber);