import React, {Component} from 'react';
import PropTypes from 'prop-types';

import QuestionBlock from './QuestionBlock';
import {getQuestions} from '../../ActionCreator/index';
import {connect} from 'react-redux';
import {TextDB} from "../../dataBases";
const textDB = new TextDB();

class QuestionList extends Component {
    static propTypes = {
        questions: PropTypes.array,
        getQuestions: PropTypes.func,
    };



    render() {
        const {questions} = this.props;

        //const type = 'bool';
        let list = [];
        for(let i = 0; i < questions.length; i++){
            list.push(
                <QuestionBlock text={questions[i].text}
                    number={questions[i].id}
                    key={questions[i].id}/>
            );
        }

        return (
            <div id='tests'>
                <div id='testList'>
                    {list}
                </div>
            </div>
        );
    }
}

export default connect(state => ({
    questions: state.questions
}), {getQuestions})(QuestionList);
