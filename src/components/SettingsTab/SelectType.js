import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getType} from '../../ActionCreator';

import UseBool from './UseBool';
import UseNum from './UseNum';

class SelectType extends Component {
    static propTypes = {
        type: PropTypes.string,
        getType: PropTypes.func
    };

    render() {
        let chackType = <div><UseBool handleClick={this.handleChangeBool} checkedD={false}/><UseNum handleClick={this.handleChangeNum} checkedD={false}/></div>;
        if(this.props.type === 'num'){
            chackType = <UseNum handleClick={this.handleChangeNum} checkedD={true}/>;
        }else if(this.props.type === 'bool'){
            chackType = <UseBool handleClick={this.handleChangeBool} checkedD={true}/>;
        }
        return (
            <div className="setting-block">
                {chackType}
            </div>
        );
    }

    handleChangeBool = () => {
        if(this.props.type === 'bool') {
            this.props.getType('none');
        }else{
            this.props.getType('bool');
        }
    };
    handleChangeNum = () => {
        if(this.props.type === 'num') {
            this.props.getType('none');
        }else{
            this.props.getType('num');
        }
    }
}

export default connect(state => ({
    type: state.type
}), {getType})(SelectType);


