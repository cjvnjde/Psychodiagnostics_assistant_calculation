'use strict';
import React, {Component} from 'react';
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {getConsideration} from "../../ActionCreator";

class SelectNum extends Component   {
    static propTypes = {
        getConsideration: PropTypes.func,
        consid: PropTypes.object,
        id: PropTypes.string
    };
    constructor(props){
        super(props);
        let num = '';
        if(this.props.consid){
            num = this.props.consid.num.join(',');
        }
        this.state = {
            numCount: num
        };
    }
    render() {
        let num = '';
        if(this.props.consid){
            num = this.props.consid.num.join(',');
        }
        return (
            <div className='use-num'>
                Номера для учета: <input type='text' onChange={this.setNum} value={num}/>
            </div>
        );
    }
    setNum = (ev) => {
        this.setState({
            numCount: ev.target.value
        });
        setTimeout(()=>{
            this.parseInput();
        },0);
    };
    parseInput = () => {
        let num = this.state.numCount.split(',').map(element => {
            if(isNaN(parseInt(element))) return;
            return parseInt(element);
        });
        this.props.getConsideration({plus:[], minus:[], num}, this.props.id);

    };

}

export default SelectNum;
