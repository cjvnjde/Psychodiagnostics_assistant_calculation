import React, {Component} from 'react';

import UploadBlock from './UploadBlock';
import SelectType from './SelectType';
import SelectQuestions from './SelectQuestions';
import SchemaBlock from './SchemaBlock';

export default class Settings extends Component {

    render() {
        return (
            <div>
                <UploadBlock />
                <SelectType />
                <SelectQuestions />
                <SchemaBlock />
            </div>

        );
    }
}

