'use strict';
import React, {Conponent} from 'react';

import store from '../../store/index';
import {Provider} from 'react-redux';

import Settings from './Settings';

function Root(props) {
    return (
        <Provider store = {store}>
            <Settings />
        </Provider>
    );
}


export default Root;