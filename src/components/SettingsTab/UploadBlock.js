import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getQuestions} from "../../ActionCreator";

class UploadBlock extends Component {
    static propTypes = {
        getQuestions: PropTypes.func,
    };

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="setting-block">
                <input type="file"  ref={(ref) => this.fileUpload = ref}/>
                <button onClick={this.handleClick}>Загрузить данные</button>
            </div>
        );
    }

    handleClick = () => {
        this.fileReader();
    };

    fileReader = () => {
        const file = this.fileUpload.files[0];

        const reader = new FileReader();
        reader.onload = () => {
            this.saveText(reader.result);
        };
        reader.readAsText(file);
    };

    saveText = (text) => {
        let arr = text.split(/\r?\n/);

        arr = arr.filter(element => {
            return element.length > 3;
        });

        arr = arr.map((element) => {
            return element.trim();
        });

        let data = [];

        for(let i = 0; i < arr.length; i++){
            data.push({text: arr[i], id: i+1});
        }
        this.props.getQuestions(data);
    }
}

export default connect(null, {getQuestions})(UploadBlock);
