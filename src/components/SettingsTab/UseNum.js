'use strict';
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {getMaxNum} from '../../ActionCreator';
import {connect} from 'react-redux';

class UseNum extends Component{
    static propTypes = {
        handleClick: PropTypes.func,
        checkedD: PropTypes.bool,
        getMaxNum: PropTypes.func,
        maxNum: PropTypes.number
    };

    render() {
        return (
            <div className="use-num">
                <p>Использовать числа <input type="checkbox"  onClick={this.props.handleClick} defaultChecked={this.props.checkedD}/></p>
                <p><input type="number" onChange={this.handleChange} value={this.props.maxNum}/></p>
            </div>
        );
    }

    handleChange = (ev) => {
        this.props.getMaxNum(parseInt(ev.target.value));
    }
}

export default connect(state=>({
    maxNum: state.maxNum
}), {getMaxNum})(UseNum);