'use strict';
import React, {Component} from 'react';
import PropTypes from 'prop-types';

class UseBool extends Component{
    static propTypes = {
        handleClick: PropTypes.func,
        checkedD: PropTypes.bool
    };

    render() {
        return (
            <div className="use-bool">
                <p>Использовать   Да/Нет <input type="checkbox" onClick={this.props.handleClick} defaultChecked={this.props.checkedD}/></p>
            </div>
        );
    }
}

export default UseBool;
