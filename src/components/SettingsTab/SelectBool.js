'use strict';
import React, {Component} from 'react';
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {getConsideration} from "../../ActionCreator";

class SelectBool extends Component   {
    static propTypes = {
        getConsideration: PropTypes.func,
        consid: PropTypes.object,
        id: PropTypes.string
    };
    constructor(props){
        super(props);
        let plus = '';
        let minus = '';
        if(this.props.consid){
            plus = this.props.consid.plus.join(',');
            minus = this.props.consid.minus.join(',');
        }
        this.state = {
            plusCount: plus,
            minusCount: minus
        };
    }
    render() {
        let plus = '';
        let minus = '';
        if(this.props.consid){
            plus = this.props.consid.plus.join(',');
            minus = this.props.consid.minus.join(',');
        }
        return (
            <div className='use-bool'>
                Номера для учета:
                <p><b>Да..</b> <input type='text' onChange={this.setPlus} value={plus}/></p>
                <p><b>Нет</b> <input type='text' onChange={this.setMinus} value={minus}/></p>
            </div>
        );
    }

    setPlus = (ev) => {
        this.setState({
            plusCount: ev.target.value
        });
        setTimeout(()=>{
            this.parseInput();
        },0);
    };

    setMinus = (ev) => {
        this.setState({
            minusCount: ev.target.value
        });
        setTimeout(()=>{
            this.parseInput();
        },0);
    };
    parseInput = () => {
        let plus = this.state.plusCount.split(',');
        plus = plus.map(element => {
            if(isNaN(parseInt(element))) return;
            return parseInt(element);
        });
        let minus = this.state.minusCount.split(',').map(element => {
            if(isNaN(parseInt(element))) return;
            return parseInt(element);
        });
        this.props.getConsideration({plus, minus, num:[]}, this.props.id);
    };
}
export default SelectBool;
