import React, {Component} from 'react';
import SelectNum from './SelectNum';
import SelectBool from './SelectBool';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {getConsideration} from "../../ActionCreator";

class SelectType extends Component {
    static propTypes = {
        getConsideration: PropTypes.func,
        type: PropTypes.string,
        count: PropTypes.number,
        consid: PropTypes.object
    };

    constructor(props){
        super(props);
        this.state = {
            count: 0
        };
    }

    render() {
        //console.log(this.props.type, this.props.statistic);
        let block = <div></div>;
        let counter = <p></p>;
        if(this.props.type==='bool'){
            block = <div><SelectBool getConsideration={this.props.getConsideration} consid={this.props.consid}/></div>;
            counter = <p>{this.props.count}</p>;
        }else if(this.props.type==='num'){
            block = <div><SelectNum getConsideration={this.props.getConsideration}/></div>;
            counter = <p>{this.props.count}</p>;
        }
        return (
            <div className='setting-block'>
                {block}
                {counter}
            </div>
        );
    }
}

export default connect(state => {
    const {statistic, type, consid} = state;
    let plusCount = 0, minusCount = 0, numCount = 0, count = 0;

    if(type === 'bool'){
        for(let i = 0; i < statistic.length; i++){
            for(let j = 0; j < consid.plus.length; j++){
                if(statistic[i].id == consid.plus[j]){
                    plusCount += statistic[i].val.plus;
                    //continue;
                }
            }
            for(let l = 0; l < consid.minus.length; l++){
                if(statistic[i].id == consid.minus[l]){
                    minusCount += statistic[i].val.minus;
                    // continue;
                }
            }
        }
        count = plusCount + minusCount;
    }else if(type == 'num'){
        for(let i = 0; i < statistic.length; i++){
            for(let j = 0; j < consid.num.length; j++){
                if(statistic[i].id == consid.num[j]){
                    numCount += statistic[i].val.num;
                    break;
                }
            }
        }
        count = numCount;
    }else {
        count = 0;
    }
    return {
        count,
        type,
        consid
    };
}, {getConsideration})(SelectType);

