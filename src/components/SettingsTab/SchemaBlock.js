import React, {Component} from 'react';
import PropTypes from 'prop-types';
import SelectNum from './SelectNum';
import SelectBool from './SelectBool';
import {connect} from 'react-redux';
import {getConsideration, getSchemas, updateSchemas} from "../../ActionCreator";
import statistic from "../../reducer/statistic";

class SchemaBlock extends Component {
    static propTypes = {
        type: PropTypes.string,
        consid: PropTypes.object,
        getConsideration: PropTypes.func,
        getSchemas: PropTypes.func,
        count: PropTypes.number,
        schemas: PropTypes.array,
        statistic: PropTypes.array,
        updateSchemas: PropTypes.func
    };

    render() {
        const {schemas, type} = this.props;
        let schemaBlocks = [];
        for(let i = 0; i < schemas.length; i++){
            if(type === 'bool'){
                let res = this.calculateSchemas(schemas[i].data);
                if(schemas[i].data.plus.length > 0 || schemas[i].data.minus.length > 0)
                    schemaBlocks.push(<div  key={schemas[i].id}><SelectBool id={schemas[i].id} getConsideration={this.schemasConsideration} consid={schemas[i].data}/><p>{res}</p></div>);
            }else if(type === 'num') {
                let res = this.calculateSchemas(schemas[i].data);
                if(schemas[i].data.num.length > 0)
                    schemaBlocks.push(<div  key={schemas[i].id}><SelectNum id={schemas[i].id} getConsideration={this.schemasConsideration} consid={schemas[i].data}/><p>{res}</p></div>);
            }
        }
        return (
            <div>
                <button onClick={this.handleClick}>Создать схему</button>
                {schemaBlocks}
            </div>
        );
    }
    //this.props.getConsideration({plus, minus, num:[]});
    schemasConsideration = (data, id) => {
        this.props.updateSchemas(id, data);
        console.log(data);
    };

    handleClick = () => {
        let schemasName = prompt('Введите название схемы');
        //console.log(this.props.consid);
        this.props.getSchemas(schemasName, this.props.consid);
    };

    calculateSchemas = (schemasData) => {
        const {statistic, type} = this.props;
        let plusCount = 0, minusCount = 0, numCount = 0, count = 0;
        let consid = schemasData;
        if(type === 'bool'){
            for(let i = 0; i < statistic.length; i++){
                for(let j = 0; j < consid.plus.length; j++){
                    if(statistic[i].id == consid.plus[j]){
                        plusCount += statistic[i].val.plus;
                        //continue;
                    }
                }
                for(let l = 0; l < consid.minus.length; l++){
                    if(statistic[i].id == consid.minus[l]){
                        minusCount += statistic[i].val.minus;
                        // continue;
                    }
                }
            }
            count = plusCount + minusCount;
        }else if(type == 'num'){
            for(let i = 0; i < statistic.length; i++){
                for(let j = 0; j < consid.num.length; j++){
                    if(statistic[i].id == consid.num[j]){
                        numCount += statistic[i].val.num;
                        break;
                    }
                }
            }
            count = numCount;
        }else {
            count = 0;
        }
        return count;
    }

}

export default connect(state => {
    const {statistic, type, consid, schemas} = state;
    return {
        statistic,
        consid,
        type,
        schemas
    };
}, {getSchemas, getConsideration, updateSchemas})(SchemaBlock);



