'use strict';
export const TEST = 'TEST';

export const GET_QUESTIONS = 'GET_QUESTIONS';

export const GET_TYPE = 'GET_TYPE';

export const GET_MAX_NUM = 'GET_MAX_NUM';

export const GET_STATISTIC = 'GET_STATISTIC';

export const GET_CONSIDERATION = 'GET_CONSIDERATION';

export const GET_SCHEMAS = 'GET_SCHEMAS';

export const DELETE_SCHEMAS = 'DELETE_SCHEMAS';

export const UPDATE_SCHEMAS = 'UPDATE_SCHEMAS';