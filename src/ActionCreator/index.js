'use strict';
import {
    GET_QUESTIONS,
    GET_TYPE,
    GET_MAX_NUM,
    GET_STATISTIC,
    GET_CONSIDERATION,
    GET_SCHEMAS,
    DELETE_SCHEMAS,
    UPDATE_SCHEMAS
} from '../constants';

export function getQuestions(questions) {
    return {
        type: GET_QUESTIONS,
        payload: {
            questions
        }
    };
}

export function getType(questType) {
    return {
        type: GET_TYPE,
        payload: {
            questType
        }
    };
}

export function getMaxNum(maxNum) {
    return {
        type: GET_MAX_NUM,
        payload: {
            maxNum
        }
    };
}

export function getStatistic(id, {minus, plus, num}) {
    return {
        type: GET_STATISTIC,
        payload: {
            statistic: {id: id, val: {plus: plus, minus: minus, num: num}}
        }
    };
}

export function getConsideration(consid) {
    return {
        type: GET_CONSIDERATION,
        payload: {
            consid: consid
        }
    };
}

export function getSchemas(schemasId, schemas) {
    return {
        type: GET_SCHEMAS,
        payload: {
            schemas: {id: schemasId, data: schemas}
        }
    };
}

export function updateSchemas(schemasId, schemas) {
    return {
        type: UPDATE_SCHEMAS,
        payload: {
            schemas: {id: schemasId, data: schemas}
        }
    };
}

export function deleteSchemas(schemasId) {
    return {
        type: DELETE_SCHEMAS,
        payload: {
            schemasId: schemasId
        }
    };
}