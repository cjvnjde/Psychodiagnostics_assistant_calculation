export default function init() {
	
    const newFormulaField = document.getElementById('new-formula-field');
    newFormulaField.addEventListener('click', ()=>createNewFormula());
}

export function createNewFormula(nameF = null, val = null) {

    document.getElementById('calc-formula').style.display = 'initial';

    const formulaContainer = document.getElementById('formula');
    let name;
    if(nameF === null){
        name = prompt('Введите название формулы', 'fu');
    }else{
        name = nameF;
    }
    if(name !== null){
        name = name.trim();
        const divName = document.createElement('div');
        divName.className = 'formula-name';
        divName.appendChild(document.createTextNode(name));
        const div = document.createElement('div');
        div.className = 'formula-container';

        const input = document.createElement('input');
        input.setAttribute('type', 'text');
        if(val !== null){
            input.value = val;
        }
        input.className = 'formula';
        const result = document.createElement('div');
        result.className = 'formula-result';
        const deleteBtn = document.createElement('button');
        deleteBtn.appendChild(document.createTextNode('Удалить'));

        deleteBtn.addEventListener('click', event => {
            deleteFormula(event);
        });
        div.appendChild(divName);
        div.appendChild(input);
        div.appendChild(result);
        div.appendChild(deleteBtn);

        formulaContainer.appendChild(div);

    }

}

function deleteFormula(event) {
	
    event.target.parentNode.parentNode.removeChild(event.target.parentNode);

    const formula = document.getElementById('formula');
    if(!formula.firstChild){
        document.getElementById('calc-formula').style.display = 'none';
    }

}