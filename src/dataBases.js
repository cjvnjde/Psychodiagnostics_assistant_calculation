import Dexie from 'dexie';

export class TextDB {
    constructor() {
        const db = new Dexie('PsychoTexts');
        db.version(1).stores({ texts: 'id,text' });
        db.open();
        this.db = db;
    }
    addToDB(data) {
        this.db.texts.put({id: data.id, text: data.text});
    }
    clearDB() {
        this.db.texts.clear();
    }
    getData(callback) {
        this.db.texts.toArray().then(data => callback(data));
    }
	
}