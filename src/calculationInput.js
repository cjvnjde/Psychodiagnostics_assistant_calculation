let data22 = null;
export default function init() {
    /*const startCountBtn = document.getElementById('start-count');
    const setNumCountList = document.getElementById('count-num');
    const setPlusCountList = document.getElementById('count-plus');
    const setMinusCountList = document.getElementById('count-minus');
    const result = document.getElementById('result');
    startCountBtn.addEventListener('click', ()=> startCount({setNumCountList, setPlusCountList, setMinusCountList}, data => {
        while(result.firstChild){
            result.removeChild(result.firstChild);
        }
        result.appendChild(document.createTextNode('Результат = '+data));
    }));*/
}


export function startCount(countLists, callback) {

    const countType = localStorage.getItem('type');
    let data = null;
    let data2 = null;
    if(countType === 'num'){
        data = createDataSet(countLists.setNumCountList);
        let numCount = createList(data);
        callback(countNum(numCount));
    }else if(countType ==='bool'){
        data = createDataSet(countLists.setPlusCountList);
        let plusCount = createList(data);

        data2 = createDataSet(countLists.setMinusCountList);
        let minusCount = createList(data2);

        callback(countMinus(minusCount) + countPlus(plusCount));
        data.whatCount = data.whatCount.concat(data2.whatCount);
    }
    data22 = data;
    hideEntries(data);
}


export function showAll() {
    const startCountBtn = document.getElementById('hide-unused');
    startCountBtn.checked = false;
    for(let i = 0; i < data22.list.length; i++){
        data22.list[i].style.display = '';
    }
}


function hideEntries(data){
    const startCountBtn = document.getElementById('hide-unused');
    if(startCountBtn.checked){
        for(let i = 0; i < data.list.length; i++){
            if(data.whatCount.includes(data.list[i].id|0)){
                data.list[i].style.display = '';
            }else{
                data.list[i].style.display = 'none';
            }
        }
    }else{
        for(let i = 0; i < data.list.length; i++){
            data.list[i].style.display = '';
        }
    }
}

function createList(data) {
    let list = [];
    console.log(data);
    for(let i = 0; i < data.list.length; i++){
        if(data.whatCount.includes(data.list[i].id|0)){
            list.push(data.list[i]);
        }
    }
    return list;
}

function createDataSet(whatCount) {
    //const whatCount = document.getElementById('what-count');
    let testList = document.getElementById('testList');
    console.log(testList);
    let whatCountList = whatCount.value.split(',');

    let NewWhatCountList = [];
    for(let i = 0; i < whatCountList.length; i++){
        const el = Number(whatCountList[i]);
        if(!isNaN(el)){
            NewWhatCountList.push(el);
        }
    }
    whatCountList = NewWhatCountList;

    if(testList !== null){
        testList = testList.childNodes;
        return {list: [...testList], whatCount: whatCountList};
    }


}


function countPlus(elementsList) {
    let count = 0;
    for(let i = 0; i < elementsList.length; i++) {
        const plusCheck = elementsList[i].querySelector('.plus-check');
        if(plusCheck.checked){
            count++;
        }
    }
    return count;
}

function countMinus(elementsList) {
    let count = 0;
    for(let i = 0; i < elementsList.length; i++) {
        const minusCheck = elementsList[i].querySelector('.minus-check');
        if(minusCheck.checked){
            count++;
        }
    }
    return count;
}

function countNum(elementsList) {
    let count = 0;
    for(let i = 0; i < elementsList.length; i++) {
        const numCheck = elementsList[i].querySelector('.num-check');
        count += numCheck.value|0;

    }
    return count;
}

