'use strict';
import {combineReducers} from 'redux';
import questions from './questions';
import type from './type';
import maxNum from './maxNum';
import statistic from './statistic';
import consid from './considerationQuestion';
import schemas from './schemas';

export default combineReducers({
    questions,
    type,
    maxNum,
    statistic,
    consid,
    schemas
});