'use strict';
import {GET_STATISTIC} from '../constants';

export default (statistic = [{id: -1, val: {plus: 0, minus: 0, num: 0}}], action) => {
    const {type, payload} = action;
    let arr = statistic.slice();

    switch (type) {
        case GET_STATISTIC: {
            arr = arr.filter(el=>{
                return el.id!==payload.statistic.id;
            })
            arr.push(payload.statistic);
            return arr;
        }
    }
    return statistic;
};