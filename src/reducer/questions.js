'use strict';
import {GET_QUESTIONS} from '../constants';

export default (questions = [{text: '', id: null}], action) => {
    const {type, payload} = action;
    switch (type) {
    case GET_QUESTIONS: {
        return payload.questions;
    }
    }
    return questions;
};