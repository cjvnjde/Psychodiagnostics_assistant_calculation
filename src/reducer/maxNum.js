'use strict';
import {GET_MAX_NUM} from '../constants';

export default (maxNum = 0, action) => {
    const {type, payload} = action;
    switch (type) {
        case GET_MAX_NUM: {
            return payload.maxNum;
        }
    }
    return maxNum;
};