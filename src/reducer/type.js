'use strict';
import {GET_TYPE} from '../constants';

export default (questType = 'none', action) => {
    const {type, payload} = action;
    switch (type) {
    case GET_TYPE: {
        return payload.questType;
    }
    }
    return questType;
};