'use strict';
import {GET_SCHEMAS, DELETE_SCHEMAS, UPDATE_SCHEMAS} from '../constants';
//добавить функционал {id: 'str', data: {plus: [], minus:[], num: []}}
export default (schemas = [], action) => {
    const {type, payload} = action;
    switch (type) {
        case GET_SCHEMAS: {
            let arr = schemas.slice();
            arr.push(payload.schemas);
            console.log(arr);
            return arr;
        }
        case UPDATE_SCHEMAS: {
            console.log(payload.schemas.id, 'id');
            let arr = schemas.slice();
            arr = arr.map(element => {
                if(element.id !== payload.schemas.id)
                    return element;
                return payload.schemas;
            });
            console.log(arr);
            return arr;
        }
        case DELETE_SCHEMAS: {
            return schemas.slice().filter(element => {
                if(element.id !== payload.schemasId)
                    return true;
                return false;
            });
        }
    }
    return schemas;
};