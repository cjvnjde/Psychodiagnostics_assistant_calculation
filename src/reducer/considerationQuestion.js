'use strict';
import {GET_CONSIDERATION} from '../constants';

export default (consid = {plus: [], minus:[], num: []}, action) => {
    const {type, payload} = action;

    switch (type) {
        case GET_CONSIDERATION: {
            return payload.consid;
        }
    }
    return consid;
};