import {fileReader, checkData} from './textLoader.js';
import {visualizer, clear} from './createText.js';
import {TextDB} from './dataBases.js';
import {createNewNumBlock, createNewPMBlock} from './createSchema.js';
import {createNewFormula} from './createFormula.js';
import {countAll} from './calculationSchema.js';
import {parseAll} from './calculationFormula.js';
import {showAll} from './calculationInput.js';

const textDB = new TextDB();
export default function init() {
    const loadBtn = document.getElementById('load-btn');
    const file = document.getElementById('load-file');

    loadBtn.addEventListener('click', () => {
        //clear();
        fileReader(file, loader);
    });
}



function loadText(data) {
    const useNumCB = document.getElementById('use-num');
    const useBoolCB = document.getElementById('use-bool');

    textDB.clearDB();
    console.log(data);
    useNumCB.checked = false;
    useBoolCB.checked = false;

    localStorage.setItem('type', data.text[0].type);
    if(data.text[0].type === 'bool'){
        useBoolCB.checked = true;
    }else if(data.text[0].type === 'num'){
        useNumCB.checked = true;
    }

    for(let i = 0; i < data.text.length; i++){
        textDB.addToDB({text: data.text[i].text, id: data.text[i].index|0 });
    }

    visualizer();
}


function loadSchema(data) {
    const countType = localStorage.getItem('type');
    for(let i = 0; i < data.schema.length; i++){
        if(data.schema[i].type === 'num'){
            createNewNumBlock(data.schema[i].num, data.schema[i].name);

        }else if(data.schema[i].type ==='bool'){
            createNewPMBlock({plus:data.schema[i].plus, minus: data.schema[i].minus}, data.schema[i].name);
        }
    }
}

function loadFormula(data){
    for(let i = 0; i < data.formula.length; i++){
        createNewFormula(data.formula[i].name, data.formula[i].formula);
    }
}

function loader(data_){
    let data = JSON.parse(data_);

    loadText(data);
    loadSchema(data);
    loadFormula(data);
    setTimeout(function() {
        countAll();
        parseAll();
        showAll();
		
    }, 100);
	
}

