
export default function init() {
    const makeSchema = document.getElementById('create-schema');
    makeSchema.addEventListener('click', () => createSchema());
}

export function createSchema() {
    const countType = localStorage.getItem('type');
    if(countType === 'num'){
        const setNumCountList = document.getElementById('count-num');
        createNewNumBlock(setNumCountList.value);

    }else if(countType ==='bool'){
        const setPlusCountList = document.getElementById('count-plus');
        const setMinusCountList = document.getElementById('count-minus');
        createNewPMBlock({plus:setPlusCountList.value, minus: setMinusCountList.value});
    }
}

function deleteSchema(event) {
    event.target.parentNode.parentNode.removeChild(event.target.parentNode);
    const schemas = document.getElementById('schemas');
    if(!schemas.firstChild){
        document.getElementById('all-count').style.display = 'none';
    }
}

export function createNewNumBlock(data, name_ = null) {
    document.getElementById('all-count').style.display = 'initial';
    const schemas = document.getElementById('schemas');
    let name = null;
    if(name_ !== null){
        name = name_;
    }else{
        name = prompt('Введите название схемы (уникальное, латиницей, без пробелов)', 'a_1');
    }
    if(name !== null){
        name = name.trim();
        const div = document.createElement('div');
        div.id = name+'-boobs';
        div.className = 'saved-schema';
        const nameDiv = document.createElement('div');
        nameDiv.className = 'name-schema';
        nameDiv.appendChild(document.createTextNode(name));

        const inputNum = document.createElement('input');
        inputNum.value = data;
        inputNum.className = 'count-num';

        const res = document.createElement('div');
        res.className = 'result';
        const deleteBtn = document.createElement('button');
        deleteBtn.appendChild(document.createTextNode('Удалить'));
        deleteBtn.addEventListener('click', event => deleteSchema(event));

        div.appendChild(nameDiv);
        div.appendChild(inputNum);
        div.appendChild(res);
        div.appendChild(deleteBtn);
		
        schemas.appendChild(div);
    }
}

export function createNewPMBlock(data, name_ = null) {
    document.getElementById('all-count').style.display = 'initial';
    const schemas = document.getElementById('schemas');

    let name = null;
    if(name_ !== null){
        name = name_;
    }else{
        name = prompt('Введите название схемы (уникальное, латиницей, без пробелов)', 'a_1');
    }
    if(name !== null){
        name = name.trim();
        const div = document.createElement('div');
        div.id = name+'-boobs';
        div.className = 'saved-schema';
        const nameDiv = document.createElement('div');
        nameDiv.className = 'name-schema';
        nameDiv.appendChild(document.createTextNode(name));

        const inputPlus = document.createElement('input');
        inputPlus.value = data.plus;
        inputPlus.className = 'count-plus';

        const inputMinus = document.createElement('input');
        inputMinus.value = data.minus;
        inputMinus.className = 'count-minus';

        const res = document.createElement('div');
        res.className = 'result';

        const deleteBtn = document.createElement('button');
        deleteBtn.appendChild(document.createTextNode('Удалить'));
        deleteBtn.addEventListener('click', event => deleteSchema(event));

        div.appendChild(nameDiv);
        div.appendChild(inputPlus);
        div.appendChild(inputMinus);
        div.appendChild(res);
        div.appendChild(deleteBtn);

        schemas.appendChild(div);
    }
}