import saver from 'file-saver';




export default function init() {
	
    const save = document.getElementById('saver-btn');
    save.addEventListener('click', () => {
        const data = {
            text: saveText(),
            schema: saveSchema(),
            formula: saveFormula()
        };
        const file = new File([JSON.stringify(data)], 'data_name.boobs', {type: 'text/plain;charset=utf-8'});
        saver.saveAs(file);
    });
}

function saveText(){
    let textBlocks = document.getElementById('testList');
    let textArr = [];
    if(textBlocks !== null){
        textBlocks = textBlocks.childNodes;
        const type = localStorage.getItem('type');
        for(let i = 0; i < textBlocks.length; i++){
            const data = {
                type: type,
                text: textBlocks[i].querySelector('.text-t').innerText,
                index: textBlocks[i].querySelector('.num-block').innerText,
            };
            textArr.push(data);
        }
    }
    return textArr;
}


function saveSchema(){
    let schemaBlocks = document.getElementById('schemas');
    let schemaArr = [];
    const type = localStorage.getItem('type');
    if(schemaBlocks !== null){
        schemaBlocks = schemaBlocks.childNodes;
        for(let i = 0; i < schemaBlocks.length; i++){
            let data = null;
            if(type === 'bool'){
                data = {
                    type: 'bool',
                    plus: schemaBlocks[i].querySelector('.count-plus').value,
                    minus: schemaBlocks[i].querySelector('.count-minus').value,
                    name: schemaBlocks[i].querySelector('.name-schema').innerText
                };
            }else if(type === 'num'){
                data = {
                    type: 'num',
                    num: schemaBlocks[i].querySelector('.count-num').value,
                    name: schemaBlocks[i].querySelector('.name-schema').innerText
                };
            }
            if(data){
                schemaArr.push(data);
            }
        }
    }
    return schemaArr;
}

function saveFormula(){
    let formulaBlocks = document.getElementById('formula');
    let formulaArr = [];
    if(formulaBlocks !== null){
        formulaBlocks = formulaBlocks.childNodes;
        for(let i = 0; i < formulaBlocks.length; i++){
            const data = {
                name: formulaBlocks[i].querySelector('.formula-name').innerText,
                formula: formulaBlocks[i].querySelector('.formula').value
            };
            formulaArr.push(data);
        }
    }
    return formulaArr;
}

