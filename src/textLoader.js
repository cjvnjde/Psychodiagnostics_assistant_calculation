import {TextDB} from './dataBases.js';

const textDB = new TextDB();

export default function init() {
    const loadBtn = document.getElementById('loader');
    const file = document.getElementById('file');

    loadBtn.addEventListener('click', () => {
        fileReader(file, saveText);
    });
}

export function checkData(){
    const dataExist = document.getElementById('data-exist');
    textDB.getData(data=>{
        if(data){
            if(dataExist.firstChild){
                dataExist.removeChild(dataExist.firstChild);
            }
            dataExist.appendChild(document.createTextNode('Данные присутствуют'));
        }
    });
}

export function fileReader(object, callback) {
    const file = object.files[0];
    /*if(file.type !== 'text/plain'){
		alert('Может быть обработан только файл формата txt')
	}else{*/
    const reader = new FileReader();
    reader.onload = function() {
        callback(reader.result);
    };
    reader.readAsText(file);
    //}
}

function saveText(text){
    let arr = text.split(/\r?\n/);

    arr = arr.filter(element => {
        return element.length > 3;
    });

    arr = arr.map((element) => {
        return element.trim();
    });

    textDB.clearDB();

    for(let i = 0; i < arr.length; i++){
        textDB.addToDB({text: arr[i], id: i+1});
    }
	
    alert('OK');
    //checkData();
}
