import {startCount} from './calculationInput.js';

export default function init() {
    const allCount = document.getElementById('all-count');
    allCount.addEventListener('click', () => {
        countAll();
    });
}

export function countAll(){
    const allSchemas = document.getElementsByClassName('saved-schema');
    const countType = localStorage.getItem('type');
    if(allSchemas !== null){
        console.log('123');
        //console.log(countType);
        if(countType === 'num'){
            for(let i = 0; i < allSchemas.length; i++){
                const num = allSchemas[i].querySelector('.count-num');
                const result = allSchemas[i].querySelector('.result');
                console.log(num);
                startCount({setNumCountList: num}, data=>{
                    result.innerText = '';
                    result.appendChild(document.createTextNode(data));
                });
            }
        }else if(countType ==='bool'){
            for(let i = 0; i < allSchemas.length; i++){
                const plus = allSchemas[i].querySelector('.count-plus');
                const minus = allSchemas[i].querySelector('.count-minus');
                const result = allSchemas[i].querySelector('.result');
                console.log(plus);
                startCount({setPlusCountList: plus, setMinusCountList: minus}, data=>{
                    result.innerText = '';
                    result.appendChild(document.createTextNode(data));
                });
            }
        }
    }
}