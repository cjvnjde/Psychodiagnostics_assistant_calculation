import React from 'react';
import {render} from 'react-dom';


//import {TextDB} from './dataBases.js';
import RootQuestions from './components/QuestionsTab/Root';
import RootSettings from './components/SettingsTab/Root';

export default function init() {
    /*const vis = document.getElementById('visualizator');
    vis.addEventListener('click', () => {
        visualizer();
    });*/
    render(<RootSettings />, document.getElementById('setReact'));
    render(<RootQuestions />, document.getElementById('test'));
/*    check();
    checkNowChecked();*/
}
/*

export function visualizer() {
    const textDB = new TextDB();
    //clearInquirer();
    textDB.getData((data)=>visualize(data));
    afterVisSetVisible();
    checkNowChecked();
}

function visualize(data) {
    const useNumCB = document.getElementById('use-num');
    const useBoolCB = document.getElementById('use-bool');

    let type = 'none';

    if(useNumCB.checked && !useBoolCB.checked){
        //div = createNumUI();
        type = 'num';
    }else if(useBoolCB.checked && !useNumCB.checked){
        //div = createPMUI();
        type = 'bool';
    }
    localStorage.setItem(type, 'num');


}


function check() {
    const useNumCB = document.getElementById('use-num');
    const useBoolCB = document.getElementById('use-bool');
	

    useNumCB.addEventListener('click', event => {
        checkNowChecked();
    });
    useBoolCB.addEventListener('click', event => {
        checkNowChecked();
		
    });
}

export function clear() {
    const schemas = document.getElementById('schemas');
    while(schemas.firstChild){
        schemas.removeChild(schemas.firstChild);
    }
    const formula = document.getElementById('formula');
    while(formula.firstChild){
        formula.removeChild(formula.firstChild);
    }
    document.getElementById('all-count').style.display = 'none';
    document.getElementById('calc-formula').style.display = 'none';
}

function checkNowChecked() {
    const useNumCB = document.getElementById('use-num');
    const useBoolCB = document.getElementById('use-bool');

    const useNumClass = document.getElementsByClassName('use-num');
    const useBoolClass = document.getElementsByClassName('use-bool');

    const maxNum = document.getElementById('max-num');
    clear();
    if(useNumCB.checked){
        [...useBoolClass].forEach(element => {
            element.style.display = 'none';
        });
        [...useNumClass].forEach(element => {
            element.style.display = '';
        });

        maxNum.style.display = 'initial';
		

		
        useBoolCB.style.disabled = true;
    }else if(useBoolCB.checked){
        [...useNumClass].forEach(element => {
            element.style.display = 'none';
        });
        [...useBoolClass].forEach(element => {
            element.style.display = '';
        });
        maxNum.style.display = 'none';
        useNumCB.style.disabled = true;
    }else{
        afterVisSetInvisible();
        [...useNumClass].forEach(element => {
            element.style.display = '';
        });
        [...useBoolClass].forEach(element => {
            element.style.display = '';
        });
        maxNum.style.display = 'none';
    }
}

function afterVisSetInvisible(){
    const afterVis = document.getElementsByClassName('after-vis');
    const useNumCB = document.getElementById('use-num');

    const useBoolCB = document.getElementById('use-bool');
    if(!useNumCB.checked || !useBoolCB.checked){
        for(let i = 0; i < afterVis.length; i++){
            afterVis[i].style.display = 'none';
        }
    }
}

function afterVisSetVisible(){
    const afterVis = document.getElementsByClassName('after-vis');
    const useNumCB = document.getElementById('use-num');
    const useBoolCB = document.getElementById('use-bool');
    if(useNumCB.checked || useBoolCB.checked){
        for(let i = 0; i < afterVis.length; i++){
            afterVis[i].style.display = 'block';
        }
    }
}*/
