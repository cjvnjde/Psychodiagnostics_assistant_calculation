module.exports = {
    'parser': 'babel-eslint',
    'parserOptions': {
        'sourceType': 'module',
        'allowImportExportEverywhere': false,
        'codeFrame': false
    },
    'env': {
        'browser': true,
        'es6': true,
        'node': true
    },
    'extends': [
        'eslint:recommended',
        'plugin:react/recommended'
    ],
    'rules': {
        'indent': [
            'warn',
            4
        ],
        'linebreak-style': [
            'error',
            'windows'
        ],
        'quotes': [
            'warn',
            'single'
        ],
        'semi': [
            'error',
            'always'
        ],
        'no-console':1,
        'no-unused-vars': 1
    },

};